from flask import Flask
from random import choice
from glob import glob
files = glob('/creator-data/*')

app = Flask(__name__)

@app.route('/')
def index():
    fp = choice(files)
    with open(fp) as fin:
        return fin.read() 

if __name__ == '__main__':
    app.run(host="0.0.0.0")
