from confluent_kafka import Producer, Consumer
from requests import get
import time
from random import randint
from json import dumps
from datetime import datetime
time.sleep(15)


def main():
    producer = Producer({'bootstrap.servers': 'kafka', 'group.id': 'mygroup'}) 
    data_service = 'http://data-creator:5000'

    while True:
        response = get(data_service)

        message = dict(timestamp=datetime.now().strftime('%m/%d/%Y %H:%M:%S'),
                        payload=response.content)
        producer.produce('test-topic', dumps(message))
        time.sleep(randint(1, 5))

if __name__ == '__main__':
    main()