from confluent_kafka import Consumer, KafkaError
from datetime import datetime
from json import loads, dump
import time
time.sleep(15)

c = Consumer({'bootstrap.servers': 'kafka', 'group.id': 'mygroup',
              'default.topic.config': {'auto.offset.reset': 'smallest'}})
c.subscribe(['test-topic'])

def main():
    running = True
    while running:
        msg = c.poll()
        if not msg.error():
            json_msg = loads(msg.value().decode('utf-8'))
            json_msg['rtimestamp'] = datetime.now().strftime('%m/%d/%Y %H:%M:%S')
            print(json_msg)
            #with open('/sql-writer/messages', 'a+') as f:
            #    f.write(json_msg.)
        elif msg.error().code() != KafkaError._PARTITION_EOF:
            print(msg.error())
            running = False
        print('Here')
    c.close()

if __name__ == '__main__':
    main()
